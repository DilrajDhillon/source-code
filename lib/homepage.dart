import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("FlyingWolf"),
        leading: GestureDetector(
          onTap: () {},
          child: Icon(
            Icons.menu,
          ),
        ),
      ),
      body: new ListView(
        children: [
          Row(
            children: [
              Column(
                children: [
                  Container(
                    width: 100,
                    height: 100,
                    margin: new EdgeInsets.only(
                      top: 20,
                      left: 20,
                    ),
                    decoration: BoxDecoration(
                        borderRadius: new BorderRadius.circular(
                          50.0,
                        ),
                        color: Colors.amber,
                        image: DecorationImage(
                          image: AssetImage(
                            'assets/simon.png',
                          ),
                          fit: BoxFit.cover,
                        )),
                    child: Image.asset(
                      'assets/simon.png',
                      height: 100,
                      width: 100,
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 0.0),
                        child: Text(
                          "Simon Baker",
                          style: new TextStyle(
                            fontSize: 28.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        height: 50,
                        width: 200,
                        margin: new EdgeInsets.only(
                          left: 10,
                          top: 10,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                            30,
                          ),
                          border: Border.all(
                            color: Colors.blue,
                          ),
                        ),
                        child: Row(
                          children: [
                            Padding(
                              padding: new EdgeInsets.only(
                                left: 18.0,
                              ),
                            ),
                            new Text(
                              "2250",
                              style:
                                  TextStyle(color: Colors.blue, fontSize: 20),
                            ),
                            Padding(
                              padding: new EdgeInsets.only(
                                left: 15.0,
                              ),
                            ),
                            new Text(
                              "Elo",
                              style:
                                  TextStyle(color: Colors.blue, fontSize: 20),
                            ),
                            Padding(
                              padding: new EdgeInsets.only(
                                left: 15.0,
                              ),
                            ),
                            new Text(
                              "rating",
                              style:
                                  TextStyle(color: Colors.blue, fontSize: 20),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              )
            ],
          ),
          Row(
            children: [
              Center(
                child: Container(
                  height: 100,
                  width: 300,
                  margin: EdgeInsets.only(
                    top: 20.0,
                    left: 30.0,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      20,
                    ),
                    border: Border.all(
                      color: Colors.blue,
                      width: 1,
                    ),
                  ),
                  child: Row(
                    children: [
                      Container(
                        height: 100,
                        width: 99,
                        decoration: BoxDecoration(
                          color: Colors.green,
                          border: Border.all(
                            color: Colors.white,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                          ),
                        ),
                        child: Column(
                          children: [
                            Padding(padding: EdgeInsets.only(top: 10)),
                            Text(
                              "34",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Center(
                              child: Padding(
                                child: Text(
                                  "Tournaments Played",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                                padding: EdgeInsets.only(left: 10),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 100,
                        width: 99,
                        decoration: BoxDecoration(
                          color: Colors.purple,
                          border: Border.all(
                            color: Colors.white,
                            width: 1,
                          ),
                        ),
                        child: Column(
                          children: [
                            Padding(padding: EdgeInsets.only(top: 10)),
                            Text(
                              "09",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Center(
                              child: Padding(
                                child: Text(
                                  "Tournaments Won",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                                padding: EdgeInsets.only(left: 10),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 100,
                        width: 99,
                        decoration: BoxDecoration(
                          color: Colors.redAccent,
                          border: Border.all(
                            color: Colors.white,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Column(
                          children: [
                            Padding(padding: EdgeInsets.only(top: 10)),
                            Text(
                              "26%",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Center(
                              child: Padding(
                                child: Text(
                                  "Winning Percentage",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                                padding: EdgeInsets.only(left: 10),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 20, left: 10),
                child: Text(
                  "Recommended for you",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            ],
          ),
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: 20, top: 20),
                width: 300,
                height: 200,
                child: Column(
                  children: [
                    Container(
                      height: 100,
                      width: 300,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                        ),
                      ),
                      child: Image.asset('assets/fortnite.jpg'),
                    ),
                    Container(
                      width: 300,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        border: Border.all(color: Colors.blue, width: 1),
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(20),
                          bottomRight: Radius.circular(20),
                        ),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 250,
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(top: 20, left: 10),
                                  child: Text(
                                    "Fortnite Champions Mar 2020",
                                    style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    child: Text(
                                      "Fortnite",
                                      style: TextStyle(color: Colors.blue),
                                    ),
                                    padding: EdgeInsets.all(20),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Icon(Icons.arrow_forward_ios),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: 20, top: 20),
                width: 300,
                height: 200,
                child: Column(
                  children: [
                    Container(
                      height: 100,
                      width: 300,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                        ),
                      ),
                      child: Image.asset('assets/fortnite.jpg'),
                    ),
                    Container(
                      width: 300,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        border: Border.all(color: Colors.blue, width: 1),
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(20),
                          bottomRight: Radius.circular(20),
                        ),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 250,
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(top: 20, left: 10),
                                  child: Text(
                                    "Clash of Clans 1v1",
                                    style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    child: Text(
                                      "Clash of Clans",
                                      style: TextStyle(color: Colors.blue),
                                    ),
                                    padding: EdgeInsets.all(20),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Icon(Icons.arrow_forward_ios),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
