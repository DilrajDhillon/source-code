import 'package:assignment/homepage.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String username = '';
  String password = '';
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
          margin: new EdgeInsets.only(),
          child: new ListView(
            children: [
              new Column(
                children: [
                  new Row(
                    children: [
                      Expanded(
                        child: Image.asset('assets/download.png'),
                      ),
                    ],
                  ),
                  new Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: new EdgeInsets.only(
                            right: 50,
                            left: 50.0,
                            top: 20.0,
                          ),
                          child: TextField(
                            onChanged: (value) {
                              username = value;
                            },
                            autofocus: true,
                            decoration: InputDecoration(hintText: "UserName"),
                          ),
                        ),
                      ),
                    ],
                  ),
                  new Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: new EdgeInsets.only(
                            right: 50,
                            left: 50.0,
                            top: 20.0,
                          ),
                          child: TextField(
                            onChanged: (value) {
                              password = value;
                            },
                            autofocus: true,
                            decoration: InputDecoration(
                              hintText: "Password",
                            ),
                            obscureText: true,
                          ),
                        ),
                      ),
                    ],
                  ),
                  new Row(
                    children: [
                      Container(
                        margin: new EdgeInsets.only(
                          left: 200,
                          top: 20,
                        ),
                        color: Colors.green,
                        padding: EdgeInsets.all(
                          10,
                        ),
                        child: Expanded(
                          child: TextButton(
                            onPressed: () => {
                              if (username == "user123" && password == '123456')
                                {
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => HomePage(),
                                      ),
                                      (route) => false)
                                }
                              else
                                {
                                  showModalBottomSheet(
                                    context: context,
                                    builder: (context) => Container(
                                      margin: EdgeInsets.only(
                                        right: 20,
                                        left: 20,
                                      ),
                                      height: 100,
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                      ),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(
                                              60,
                                            ),
                                            topRight: Radius.circular(
                                              60,
                                            ),
                                          ),
                                        ),
                                        child: ListView(
                                          children: [
                                            Text(
                                              "UserName " + username,
                                            ),
                                            Text(
                                              " Password " + password,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                },
                            },
                            child: new Text(
                              "LOGIN",
                              style: new TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ],
          )),
    );
  }
}
