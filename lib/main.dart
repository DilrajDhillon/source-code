import 'package:assignment/homepage.dart';
import 'package:assignment/login.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(Home());
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
          accentColor: Colors.brown.shade50,
          primaryColor: Colors.brown.shade100),
      debugShowCheckedModeBanner: false,
      home: Login(),
    );
  }
}
